package sample.connect.spring.dynamodb.dynamo;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Optional;

@Component
class DynamoAtlassianHostRepository implements AtlassianHostRepository {

    private final DynamoAtlassianHostCrudRepository dynamoHostRepository;
    private final AtlassianHostUserAuditorAware auditorAware;

    DynamoAtlassianHostRepository(DynamoAtlassianHostCrudRepository dynamoHostRepository,
                                  AtlassianHostUserAuditorAware auditorAware) {
        this.dynamoHostRepository = dynamoHostRepository;
        this.auditorAware = auditorAware;
    }

    @Override
    public Optional<AtlassianHost> findFirstByBaseUrl(String baseUrl) {
        return dynamoHostRepository.findFirstByBaseUrl(baseUrl);
    }

    @Override
    public AtlassianHost save(AtlassianHost atlassianHost) {
        setAuditingFields(atlassianHost);
        final DynamoAtlassianHost dynamoAtlassianHost = DynamoAtlassianHost.fromAtlassianHost(atlassianHost);
        return dynamoHostRepository.save(dynamoAtlassianHost);
    }

    private void setAuditingFields(AtlassianHost atlassianHost) {
        // DynamoDB doesn't support auditing like other Spring Data implementations, e.g.:
        // - EnableJpaAuditing
        // - EnableMongoAuditing
        final Calendar now = Calendar.getInstance();
        final String currentAuditor = auditorAware.getCurrentAuditor();
        if (atlassianHost.getCreatedDate() == null) {
            atlassianHost.setCreatedDate(now);
            atlassianHost.setCreatedBy(currentAuditor);
        }
        atlassianHost.setLastModifiedDate(now);
        atlassianHost.setLastModifiedBy(currentAuditor);
    }

    @Override
    public <S extends AtlassianHost> Iterable<S> save(Iterable<S> atlassianHosts) {
        throw new UnsupportedOperationException("save(atlassianHosts)");
    }

    @Override
    public AtlassianHost findOne(String id) {
        return Optional.ofNullable(dynamoHostRepository.findOne(id)).orElse(null);
    }

    @Override
    public boolean exists(String id) {
        return dynamoHostRepository.exists(id);
    }

    @Override
    public Iterable<AtlassianHost> findAll() {
        throw new UnsupportedOperationException("findAll()");
    }

    @Override
    public Iterable<AtlassianHost> findAll(Iterable<String> ids) {
        throw new UnsupportedOperationException("findAll(ids)");
    }

    @Override
    public long count() {
        return dynamoHostRepository.count();
    }

    @Override
    public void delete(String id) {
        dynamoHostRepository.delete(id);
    }

    @Override
    public void delete(AtlassianHost atlassianHost) {
        final DynamoAtlassianHost dynamoAtlassianHost = DynamoAtlassianHost.fromAtlassianHost(atlassianHost);
        dynamoHostRepository.delete(dynamoAtlassianHost);
    }

    @Override
    public void delete(Iterable<? extends AtlassianHost> atlassianHosts) {
        atlassianHosts.forEach(this::delete);
    }

    @Override
    public void deleteAll() {
        dynamoHostRepository.deleteAll();
    }
}
