package sample.connect.spring.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;

interface MyselfClient {

    @GET("rest/api/2/myself")
    Call<User> getMyself();
}
