package sample.connect.spring.jersey;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

@RestController
public class JwtDataController {

    @Value("${addon.key}")
    private String addonKey;

    @Autowired
    private JwtClientRequestFilter jwtClientRequestFilter;

    @IgnoreJwt
    @GetMapping("/jwt-data")
    public String getData(@RequestParam String hostBaseUrl) {
        Client client = ClientBuilder.newClient();
        client.register(jwtClientRequestFilter);
        return client.target(hostBaseUrl)
                .path("rest/atlassian-connect/1/addons/{addon-key}")
                .resolveTemplate("addon-key", addonKey)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);
    }
}

